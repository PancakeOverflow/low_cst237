﻿namespace MinesweeperConsole
{
    interface IPlayable
    {
        void playGame(int x, int y, int mines);
    }
}
