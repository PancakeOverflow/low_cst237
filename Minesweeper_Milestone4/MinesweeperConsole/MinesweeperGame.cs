﻿using System;

namespace MinesweeperConsole
{
    class MinesweeperGame : Grid, IPlayable
    {
        private bool gameOver = true;
        private int cellCount;

        /* METHODS */

        // playGame
        public void playGame(int x, int y, int mines)
        {
            gameOver = false;
            cellCount = x * y;
            Populate();
            runGame();
            if (!gameOver)
            {
                revealAll();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("WINNAH!");
            }
        }
        private void runGame()
        {
            // begin loop
            while (!gameOver && !checkForWin())
            {
                // reveal board
                revealGrid();
                // take user input
                Console.WriteLine("Type your move:");
                int move_x = getX();
                int move_y = getY();
                Cell cell = CellArray[move_x, move_y];
                // if cell already visited, return errorString, end iteration
                if (cell.Visited)
                    Console.WriteLine("Already checked!");
                // else if cell is live, return looseString, end loop
                else if (cell.Live)
                    gameLose();
                markCell(move_x, move_y);
                if (cell.Adjacent == 0)
                    markArea(move_x, move_y);
            }
        }

        private bool checkForWin()
        {
            int cell_num = Height * Width;
            foreach (Cell cell in CellArray)
            {
                if (cell.Visited)
                    cell_num--;
            }
            return cell_num - Mines == 0;
        }

        private void gameLose()
        {
            gameOver = true;
            revealAll();
            Console.WriteLine("Oh noes! You stepped on a mine. \nGame-over.");
        }
        
        private int getY()
        {
            int move = -1;
            while (move < 0)
            {
                if (move == -2)
                    Console.WriteLine("Must be between 1 and " + Height);
                Console.WriteLine("Y - ");
                int temp;
                if (Int32.TryParse(Console.ReadLine(), out temp))
                {
                    temp--;
                    move = (temp > -1 && temp < Height) ? temp : -2;
                }
                else move = -2;
            }
            return move;
        }

        private int getX()
        {
            int move = -1;
            while (move < 0)
            {
                if (move == -2)
                    Console.WriteLine("Must be between 1 and " + Width);
                Console.WriteLine("X - ");
                int temp;
                if (Int32.TryParse(Console.ReadLine(), out temp))
                {
                    temp--;
                    move = (temp > -1 && temp < Width) ? temp : -2;
                }
                else move = -2;
            }
            return move;
        }

        /* Constructor */
        public MinesweeperGame(int width, int height, int mines)
            : base(width, height, mines)
        {
            playGame(width, height, mines);
        }

        public override void revealGrid()
        {
            Console.Clear();
            // Number columns
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("\n   ");
            for (int i = 0; i < Width; i++)
            {
                if (i < 9)
                    Console.Write(" " + (i + 1) + " ");
                else
                    Console.Write(" " + (i + 1));
            }
            Console.WriteLine();
            for (int y = 0; y < Height; y++) // column
            {
                // Number row
                Console.ForegroundColor = ConsoleColor.White;
                if (y < 9)
                    Console.Write((y + 1) + "  ");
                else
                    Console.Write((y + 1) + " ");
                for (int x = 0; x < Width; x++) // row
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Cell cell = CellArray[x, y];
                    // if !grid.cellArray[arg_x,arg_y].visted print "?"
                    if (!cell.Visited)
                        Console.Write(" ? ");
                    // else if adjacent > 0 print adjacent
                    else if (cell.Adjacent > 0 && !cell.Live)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write(" " + cell.Adjacent + " ");
                    }
                    // else (if empty) recursively print all surrounding cells
                    else if (cell.Live)
                        Console.Write(" * ");
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(" ~ ");
                    }
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.WriteLine("\n");
            }
        }

        private void revealAll()
        {
            foreach (Cell cell in CellArray)
            {
                cell.Visited = true;
            }
            revealGrid();
        }
    } // class
} // namespace
