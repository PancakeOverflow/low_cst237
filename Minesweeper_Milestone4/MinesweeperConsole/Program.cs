﻿using System;

namespace MinesweeperConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            test();
        }

        static void test()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Minesweeper - New game\nChoose size (s, m, l):");
            String response = Console.ReadLine();
            Console.WriteLine(response.ToLower() + ".");
            while (response.ToLower() != "s" && response.ToLower() != "m" && response.ToLower() != "l")
            {
                Console.WriteLine("Please choose from the following sizes: s(mall), m(edium), l(arge).");
                response = Console.ReadLine();
            }
            MinesweeperGame game;
            if (response.ToLower() == "s")
                game = new MinesweeperGame(8, 6, 8);
            else if(response.ToLower() == "m")
                game = new MinesweeperGame(12, 10, 20);
            else
                game = new MinesweeperGame(18, 14, 42);
            Console.WriteLine("New Game (y/n)?");
            if (Console.ReadLine() != "n")
                test();
        }
    }
}
