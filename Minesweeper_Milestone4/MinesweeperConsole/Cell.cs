﻿// This is my original work

namespace MinesweeperConsole
{
    class Cell
    {
        // --- FIELDS ---
        public bool Visited { get; set; } = false;
        public bool Flagged { get; set; } = false;
        // auto properties
        public bool Live { get; set; } = false;
        public int Adjacent { get; set; } = 0;

        //  --- METHODS --- 
    }
}
