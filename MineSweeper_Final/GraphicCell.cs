using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MinesweeperConsole;

//using MinesweeperConsole;

namespace MineSweeper_Final
{
    class GraphicCell : Button
    {
        public int Clicks { get; set; }
        public new int Size { get; set; }
        public MinesweeperGame Game { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Status { get; set; }
        public Cell CellLogic;
        public Stopwatch GameTimer { get; set; }

        public GraphicCell(int size, MinesweeperGame game, int x, int y)
        {
            GameTimer = new Stopwatch();
            GameTimer.Start();
            BackColor = ColorTranslator.FromHtml("#40183E"); // dark purple
            FlatStyle = FlatStyle.Flat;
            FlatAppearance.BorderSize = 1;
            FlatAppearance.BorderColor = ColorTranslator.FromHtml("#0F060F");
            Size = size;
            Height = size;
            Width = size;
            Game = game;
            CellLogic = Game.CellArray[x, y];
            X = x;
            Y = y;

            MouseDown += ButtonClick;
        }

        public void setImage()
        {
            if (CellLogic.Visited)
            {
                Text = "";
                if (CellLogic.Live)
                {
                    Image = Image.FromFile("../bomb-icon.png");
                    if (Status == 21)
                        BackColor = Color.DarkSlateGray;
                    else
                        BackColor = Color.Black;
                }
                else
                {
                    switch (CellLogic.Adjacent)
                    {
                        case 1:
                            BackColor = ColorTranslator.FromHtml("#6E0041");
                            break;
                        case 2:
                            BackColor = ColorTranslator.FromHtml("#820333");
                            break;
                        case 3:
                            BackColor = ColorTranslator.FromHtml("#C9283E");
                            break;
                        case 4:
                            BackColor = ColorTranslator.FromHtml("#F0433A");
                            break;
                        case 5:
                            BackColor = ColorTranslator.FromHtml("#B53316");
                            break;
                        case 6:
                            BackColor = ColorTranslator.FromHtml("#FF5C38");
                            break;
                        case 7:
                            BackColor = ColorTranslator.FromHtml("#691806");
                            break;
                        case 8:
                            BackColor = ColorTranslator.FromHtml("#CC150E");
                            break;
                        default:
                            BackColor = ColorTranslator.FromHtml("#1C080F");
                            break;
                    }
                    if (CellLogic.Adjacent > 0)
                        Text = "" + CellLogic.Adjacent;
                }
                Enabled = false;
            }
            else if (CellLogic.Flagged)
            {
                Text = "?";
                BackColor = ColorTranslator.FromHtml("#428F27"); // green
            }
            else if (!CellLogic.Flagged)
            {
                Text = "";
                BackColor = ColorTranslator.FromHtml("#40183E"); // dark purple - initial color
            }
        }

        public void ButtonClick(Object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) // Check Cell
            {
                Status = Game.move(X, Y);
                setImage();

                GridGUI parent = (GridGUI)Parent;
                if (Status >= 10)
                {
                    Game.markArea(X, Y);
                    if (Status >= 20)
                    {
                        Game.markAll();
                    }
                }
                if (Status == 20)
                {
                    parent.refreshCells();
                    MessageBox.Show("Game Over. You stepped on a mine.");
                }
                else
                {
                    if (Status == 11)
                        parent.refreshCells();
                    if (Status == 21)
                    {
                        parent.refreshCells();
                        int time = (int)GameTimer.ElapsedMilliseconds / 1000;
                        var minutes = time >= 60 ? "" + time / 60 : "0";
                        var seconds = time % 60 > 9 ? "" + time % 60 : "0" + time % 60;
                        MessageBox.Show("You win! Time: " + minutes + ":" + seconds);
                        ScoreManager manager = new ScoreManager(null);
                        if (time < manager.getMinRank().Time)
                        {
                            Name name = new Name();
                            name.Show();
                            var ins = name.Initials;
                            PlayerStats stat = new PlayerStats(ins, Game.GameType, time);
                            Game.win(stat);
                            var hs = new HighScore();
                            hs.Show();
                        }
                    }
                }
                Enabled = false;
            }
            else if (e.Button == MouseButtons.Right) // Flag Cell
            {
                Game.flagCell(X, Y);
                refresh();
            }


        } // handler

        public void refresh()
        {
            setImage();
        }

    } // class
} // namespace
