﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MineSweeper_Final;

namespace MineSweeper_Final
{
    public partial class Name : Form
    {
        public String Initials { get; set; }
        public MinesweeperGame Game { get; set; }
        public int Time { get; set; }

        public Name(MinesweeperGame game, int time)
        {
            Game = game;
            Time = time;
            InitializeComponent();
            Initials = "---";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MinimizeBox = true;
            Initials = InputBox.Text.Length > 0 ? InputBox.Text : "gst";
            PlayerStats stat = new PlayerStats(Initials, Game.GameType, Time);
            Game.win(stat);
            var hs = new HighScore(Time, Game);
            hs.Show();
            MinimizeBox = true;
        }
    }
}
