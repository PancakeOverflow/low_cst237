﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MineSweeper_Final
{
    public class ScoreManager
    {
        private string connect =
                @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\source\repos\MineSweeper_Final\MineSweeper_Final\bin\Release\HighScores.mdf;Integrated Security=True";
        public PlayerStats[] Stats { get; set; }
        public string GameType { get; set; }

        public ScoreManager(string gameType)
        {
            GameType = gameType;
            Stats = getStats();
        }

        public PlayerStats getMinRank()
        {
            PlayerStats stat = null;
            using (var conn = new SqlConnection(connect))
            {
                try
                {
                    String db;
                    switch (GameType)
                    {
                        case "small":
                            db = "Small";
                            break;
                        case "medium":
                            db = "Medium";
                            break;
                        case "large":
                            db = "Large";
                            break;
                        default:
                            db = "Custom";
                            break;
                    }
                    var cmd = new SqlCommand(@"select * from " + db + @" where place = '10';")
                    {
                        Connection = conn
                    };

                    // Get results
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        stat = new PlayerStats((string)reader["name"], GameType, (int)reader["time"]);
                    }

                }
                catch (Exception)
                {
                    Console.WriteLine(@"SQL error");
                    throw;
                }
                finally
                {
                    try
                    {
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine(@"Connection error");
                        throw;
                    }
                }
            }
            return stat ?? new PlayerStats("---", GameType, 9999);
        }

        public PlayerStats[] getStats()
        {
            var stats = new PlayerStats[10];
            using (var conn = new SqlConnection(connect))
            {
                try
                {
                    String db;
                    switch (GameType)
                    {
                        case "small":
                            db = "Small";
                            break;
                        case "medium":
                            db = "Medium";
                            break;
                        case "large":
                            db = "Large";
                            break;
                        default:
                            db = "Custom";
                            break;
                    }
                    var query = @"select * from " + db;
                    var cmd = new SqlCommand(query)
                    {
                        Connection = conn
                    };

                    // Get results
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    int i = 0;
                    while (reader.Read())
                    {
                        var stat = new PlayerStats(reader.GetString(reader.GetOrdinal("name")),
                            GameType, reader.GetInt32(reader.GetOrdinal("time")));
                        stats[i++] = stat;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine(@"SQL error");
                    throw;
                }
                finally
                {
                    try
                    {
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine(@"Connection error");
                        throw;
                    }
                }
            }
            for (int i = 0; i < 10; i++)
            {
                if (stats[i] == null || stats[i].Time == 0)
                {
                    stats[i] = new PlayerStats("---", GameType, 9999);
                    add(stats[i], i + 1);
                }
            }
            return stats;
        }

        public void add(PlayerStats stat, int place)
        {
            using (var conn = new SqlConnection(connect))
            {
                try
                {
                    String db;
                    switch (stat.Size)
                    {
                        case "small":
                            db = "Small";
                            break;
                        case "medium":
                            db = "Medium";
                            break;
                        case "large":
                            db = "Large";
                            break;
                        default:
                            db = "Custom";
                            break;
                    }
                    conn.Open();
                    var query = "insert into " + db + "(place, name, time)" +
                                " values ('" + place + "', '" + stat.Name + "', '" + stat.Time + "');";
                    var cmd = new SqlCommand(query)
                    {
                        Connection = conn
                    };
                    cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    Console.WriteLine(@"SQL error in add");
                }
                finally
                {
                    try
                    {
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine(@"Connection error in Update");
                        throw;
                    }
                }
            }
        }

        public void update()
        {
            using (var conn = new SqlConnection(connect))
            {
                try
                {
                    conn.Open();
                    int place = 1;
                    foreach (PlayerStats stat in Stats)
                    {
                        String db;
                        switch (stat.Size.ToLower())
                        {
                            case "small":
                                db = "Small";
                                break;
                            case "medium":
                                db = "Medium";
                                break;
                            case "large":
                                db = "Large";
                                break;
                            default:
                                db = "Custom";
                                break;
                        }
                        var query = "update " + db + " set " +
                                    "name = '" + stat.Name + "', " +
                                    "time = '" + stat.Time + "' " +
                                    "where place = '" + place + "'; " +
                                    "delete from HighScores where place = " + place + "; " +
                                    "insert into HighScores(place, name, time, size) " +
                                    "values(" + place + ", '" + stat.Name + "', " + stat.Time + ", '" + db + "');";
                        place++;

                        var cmd = new SqlCommand(query)
                        {
                            Connection = conn
                        };
                        var reader = cmd.ExecuteReader();
                        reader.Close();
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine(@"SQL error in Update");
                    throw;
                }
                finally
                {
                    try
                    {
                        conn.Close();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine(@"Connection error in Update");
                        throw;
                    }
                }
            }
        }

        public void challenge(PlayerStats challenger)
        {
            var found = false;
            for (int i = 0; i < 10; i++)
            {
                if (challenger.CompareTo(Stats[i]) == -1 || found)
                {
                    PlayerStats swp = Stats[i];
                    Stats[i] = challenger;
                    challenger = swp;
                    found = true;
                }
            }
        }

        public PlayerStats[] SortTop10(int min, int max)
        {
            var input = new PlayerStats[max - min + 1];
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = Stats[min + i];
            }
            if (max == min)
                return input;
            var output = new PlayerStats[max - min + 1];
            var top = SortTop10(min + (max / 2), max); // Top half
            var bottom = SortTop10(min, min + (max / 2) - 1); // Bottom half

            // sort
            int b = 0;
            int t = 0;
            int o = 0;
            while (t < top.Length && b < bottom.Length)
            {
                if (top[t].CompareTo(bottom[b]) == -1)
                    output[o++] = top[t++];
                else
                    output[o++] = bottom[b++];
            }
            while (t < top.Length)
                output[o++] = top[t++];
            while (b < bottom.Length)
                output[o++] = bottom[b++];

            return output;
        }

    }
}