﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSweeper_Final
{
    public partial class StartMenu : Form
    {
        public StartMenu()
        {
            InitializeComponent();
        }

        private void ButtonSmallGrid_Click(object sender, EventArgs e)
        {
            MinimizeBox = true;
            var g = new GridGUI(10, 10, "small");
            g.Show();
        }

        private void ButtonMediumGrid_Click(object sender, EventArgs e)
        {
            MinimizeBox = true;
            var g = new GridGUI(18, 15, "medium");
            g.Show();
        }

        private void ButtonLargeGrid_Click(object sender, EventArgs e)
        {
            MinimizeBox = true;
            var g = new GridGUI(25, 20, "large");
            g.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MinimizeBox = true;
            var rows = textBox1.Text;
            var cols = textBox2.Text;
            if (int.TryParse(rows, out int x))
                if (int.TryParse(cols, out var y))
                {
                    if (x > 40 || y > 22)
                    {
                        MessageBox.Show("Please select a smaller size (MAX: 40 x 22)");

                    }
                    else if (x < 5 || y < 5)
                    {
                        MessageBox.Show("Please select a smaller size (MIN: 5 x 5) ");
                    }
                    else
                    {
                        var g = new GridGUI(x, y, "custom");
                        g.Show();
                    }
                }
        }
    }

}
