﻿namespace MineSweeper_Final
{
    interface IPlayable
    {
        void playGame();
    }
}
