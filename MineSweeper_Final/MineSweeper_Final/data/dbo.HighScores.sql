﻿CREATE TABLE [dbo].[HighScores]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[name] varchar(3) not null,
	[size] varchar(9) not null,
	[time] int not null
)
