﻿using System;

// by Connor
// Version 2.1
namespace MineSweeper_Final
{
    public abstract class Grid
    {
        // --- FIELDS ---
        public Cell[,] CellArray { get; set; }
        // setters and getters
        public int Width { get; set; }

        public int Height { get; set; }

        public int Mines { get; set; }

        // --- METHODS ---
        // Constructor
        public Grid(int width, int height, int mines)
        {
            Width = width;
            Height = height;
            Mines = mines;

            CellArray = new Cell[Width, Height];
        }

        // RECURSION METHODS: {

        public void markArea(int argX, int argY)
        {
            markCell(argX, argY);
            checkNext(argX - 1, argY);
            checkNext(argX - 1, argY + 1);
            checkNext(argX - 1, argY - 1);
            checkNext(argX + 1, argY);
            checkNext(argX + 1, argY + 1);
            checkNext(argX + 1, argY - 1);
            checkNext(argX, argY + 1);
            checkNext(argX, argY - 1);
        }

        public void checkNext(int arg_x, int arg_y)
        {
            if (inBounds(arg_x, arg_y) && !CellArray[arg_x, arg_y].Visited)
            {
                if (CellArray[arg_x, arg_y].Adjacent > 0)
                    markCell(arg_x, arg_y);
                else
                    markArea(arg_x, arg_y);
            }
        }

        // }

        public void markCell(int x, int y)
        {
            CellArray[x, y].Visited = true;
        }

        public void markAll()
        {
            foreach (var cell in CellArray)
            {
                cell.Visited = true;
            }
        }

        public void flagCell(int x, int y)
        {
            CellArray[x, y].Flagged = true;
        }

        public void unflagCell(int x, int y)
        {
            CellArray[x, y].Flagged = false;
        }

        public bool Populate()
        {
            // add Adjacent
            // fill cellArray with Cell objects
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    CellArray[x, y] = new Cell();
                }
            // arm live cells
            for (int m = 0; m < Mines; m++)
            {
                Random rnd = new Random();
                int x = rnd.Next(Width);
                int y = rnd.Next(Height);
                while (CellArray[x, y].Live)
                {
                    x = rnd.Next(Width);
                    y = rnd.Next(Height);
                }
                CellArray[x, y].Live = true;
                for (int rX = x - 1; rX <= x + 1 && rX < Width; rX++)
                {
                    for (int rY = y - 1; rY <= y + 1 && rY < Height; rY++)
                    {
                        if (rX > -1 && rY > -1)
                            CellArray[rX, rY].Adjacent++;
                    }
                }
            }
            return true;
        }

        private bool inBounds(int x, int y)
        {
            return y != -1 && y < Height && x != -1 && x < Width;
        }

    } // class
} // namespace
