﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSweeper_Final
{
    public partial class HighScore : Form
    {
        public int Time { get; set; }
        public MinesweeperGame Game { get; set; }
        public PlayerStats[] Stats { get; set; }

        public HighScore(int time, MinesweeperGame game)
        {
            Game = game;
            Time = time;
            InitializeComponent();
            Stats = new ScoreManager(Game.GameType).getStats();
        }

        private void HighScore_Load(object sender, EventArgs e)
        {
            Refresh();
            LoadData();
            scoreLabel.Text = Time > 0
                ? "Your Time: " + Time + "s for " + Game.GameType + " game."
                : "Your Time: none - unsuccessful";

        }

        private void LoadData()
        {
            highScoresDataSet.Clear();
            highScoresDataSet.HighScores.Clear();
            highScoresDataSet.GetChanges();
            highScoresDataSet.HighScores.GetChanges();
            highScoresDataSet.AcceptChanges();
            highScoresDataSet.HighScores.AcceptChanges();
            highScoresTableAdapter.Fill(highScoresDataSet.HighScores);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            LoadData();
        }

    }
}
