﻿using System;
using System.Diagnostics;
using MineSweeper_Final;

namespace MineSweeper_Final
{
    public class MinesweeperGame : Grid, IPlayable
    {
        private bool GameOver { get; set; } = true;
        public string GameType { get; set; }


        /* METHODS */

        // playGame
        public void playGame()
        {
            GameOver = false;
            Populate();
            if (GameOver)
            {
                revealGrid();
            }
        }

        public int move(int x, int y)
        {
            if (CellArray[x, y].Adjacent == 0)
            {
                markArea(x, y);
                if (countCells() == 0)
                    return 21; // win
                return 11; // refresh all
            }
            markCell(x, y);
            if (CellArray[x, y].Live)
            {
                return 20; // hit mine
            }
            if (countCells() == 0)
                return 21; // win
            return 1; // safe move
        }

        public int flag(int x, int y)
        {
            if (CellArray[x, y].Flagged)
            {
                unflagCell(x, y);
                return 0;
            }
            flagCell(x, y);
            return 1;
        }

        public bool checkForWin()
        {
            int cell_num = Height * Width;
            foreach (Cell cell in CellArray)
            {
                if (cell.Visited)
                    cell_num--;
            }
            return cell_num - Mines == 0;
        }

        /* Constructor */
        public MinesweeperGame(int width, int height, int mines)
            : base(width, height, mines)
        {
        }

        public void revealGrid()
        {
            foreach (Cell cell in CellArray)
            {
                cell.Visited = true;
            }
        }

        private int countCells()
        {
            int result = Width * Height - Mines;
            foreach (Cell cell in CellArray)
            {
                if (cell.Visited)
                    result--;
            }
            return result;
        }

        internal bool check(int time)
        {
            ScoreManager manager = new ScoreManager(GameType);
            return time < manager.getMinRank().Time;
        }

        internal void win(PlayerStats stat)
        {
            ScoreManager manager = new ScoreManager(stat.Size);

            manager.challenge(stat);
            manager.update();


        }

        internal void end(string size)
        {
            ScoreManager manager = new ScoreManager(size);
            manager.update();
        }
    } // class
} // namespace
