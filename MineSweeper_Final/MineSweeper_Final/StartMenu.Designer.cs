﻿namespace MineSweeper_Final
{
    partial class StartMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param Name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ButtonLargeGrid = new System.Windows.Forms.Button();
            this.ButtonMediumGrid = new System.Windows.Forms.Button();
            this.ButtonSmallGrid = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.ButtonLargeGrid);
            this.panel1.Controls.Add(this.ButtonMediumGrid);
            this.panel1.Controls.Add(this.ButtonSmallGrid);
            this.panel1.Location = new System.Drawing.Point(200, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(412, 595);
            this.panel1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(15, 429);
            this.textBox1.Margin = new System.Windows.Forms.Padding(15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(179, 35);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.AccessibleDescription = "";
            this.button1.AccessibleName = "";
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(197)))));
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.button1.Location = new System.Drawing.Point(93, 504);
            this.button1.Margin = new System.Windows.Forms.Padding(25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(229, 66);
            this.button1.TabIndex = 0;
            this.button1.Text = "Custom";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtonLargeGrid
            // 
            this.ButtonLargeGrid.AccessibleDescription = "";
            this.ButtonLargeGrid.AccessibleName = "";
            this.ButtonLargeGrid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonLargeGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.ButtonLargeGrid.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(197)))));
            this.ButtonLargeGrid.FlatAppearance.BorderSize = 2;
            this.ButtonLargeGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLargeGrid.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonLargeGrid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.ButtonLargeGrid.Location = new System.Drawing.Point(93, 272);
            this.ButtonLargeGrid.Margin = new System.Windows.Forms.Padding(25);
            this.ButtonLargeGrid.Name = "ButtonLargeGrid";
            this.ButtonLargeGrid.Size = new System.Drawing.Size(229, 66);
            this.ButtonLargeGrid.TabIndex = 0;
            this.ButtonLargeGrid.Text = "Large";
            this.ButtonLargeGrid.UseVisualStyleBackColor = false;
            this.ButtonLargeGrid.Click += new System.EventHandler(this.ButtonLargeGrid_Click);
            // 
            // ButtonMediumGrid
            // 
            this.ButtonMediumGrid.AccessibleDescription = "";
            this.ButtonMediumGrid.AccessibleName = "";
            this.ButtonMediumGrid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonMediumGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.ButtonMediumGrid.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(197)))));
            this.ButtonMediumGrid.FlatAppearance.BorderSize = 2;
            this.ButtonMediumGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMediumGrid.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonMediumGrid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.ButtonMediumGrid.Location = new System.Drawing.Point(93, 156);
            this.ButtonMediumGrid.Margin = new System.Windows.Forms.Padding(25);
            this.ButtonMediumGrid.Name = "ButtonMediumGrid";
            this.ButtonMediumGrid.Size = new System.Drawing.Size(229, 66);
            this.ButtonMediumGrid.TabIndex = 0;
            this.ButtonMediumGrid.Text = "Medium";
            this.ButtonMediumGrid.UseVisualStyleBackColor = false;
            this.ButtonMediumGrid.Click += new System.EventHandler(this.ButtonMediumGrid_Click);
            // 
            // ButtonSmallGrid
            // 
            this.ButtonSmallGrid.AccessibleDescription = "";
            this.ButtonSmallGrid.AccessibleName = "";
            this.ButtonSmallGrid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonSmallGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.ButtonSmallGrid.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(197)))));
            this.ButtonSmallGrid.FlatAppearance.BorderSize = 2;
            this.ButtonSmallGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSmallGrid.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSmallGrid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(43)))), ((int)(((byte)(36)))));
            this.ButtonSmallGrid.Location = new System.Drawing.Point(93, 40);
            this.ButtonSmallGrid.Margin = new System.Windows.Forms.Padding(25);
            this.ButtonSmallGrid.Name = "ButtonSmallGrid";
            this.ButtonSmallGrid.Size = new System.Drawing.Size(229, 66);
            this.ButtonSmallGrid.TabIndex = 0;
            this.ButtonSmallGrid.Text = "Small";
            this.ButtonSmallGrid.UseVisualStyleBackColor = false;
            this.ButtonSmallGrid.Click += new System.EventHandler(this.ButtonSmallGrid_Click);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(218, 429);
            this.textBox2.Margin = new System.Windows.Forms.Padding(15);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(179, 35);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 397);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Rows";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Columns";
            // 
            // StartMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 753);
            this.Controls.Add(this.panel1);
            this.Name = "StartMenu";
            this.Text = "StartMenu";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ButtonLargeGrid;
        private System.Windows.Forms.Button ButtonMediumGrid;
        private System.Windows.Forms.Button ButtonSmallGrid;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }

}

