﻿using System;

namespace MineSweeper_Final
{
    public class PlayerStats : IComparable<PlayerStats>
    {
        public String Name { get; set; }
        public String Size { get; set; }
        public int Time { get; set; }

        public PlayerStats(String name, String size, int time)
        {
            Name = name;
            Size = size;
            Time = time;
        }

        public int CompareTo(PlayerStats obj)
        {
            int cvar = obj.Time;
            if (cvar > Time)
                return -1;
            if (cvar < Time)
                return 1;
            if (cvar == Time)
                return 0;
            return -10;
        }

    }
}