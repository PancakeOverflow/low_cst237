﻿using System.Drawing;
using System.Windows.Forms;

namespace MineSweeper_Final
{
    public partial class GridGUI : Form
    {
        public GraphicCell[,] Grid { get; set; }
        public MinesweeperGame GridLogic { get; set; }

        public GridGUI(int x, int y, string type)
        {
            int size = 100;
            while (size * y >= 800 || size * x >= 1500)
            {
                size--;
            }
            Grid = new GraphicCell[x, y];
            var mines = (x * y * 15) / 100 - 2;
            GridLogic = new MinesweeperGame(x, y, mines);
            GridLogic.GameType = type;
            GridLogic.Populate();
            InitializeComponent();
            AutoSize = true;
            for (int row = 0; row < y; row++)
            {
                for (int col = 0; col < x; col++)
                {
                    Grid[col, row] = new GraphicCell(size, GridLogic, col, row);
                    Controls.Add(Grid[col, row]);
                    Grid[col, row].Location = new Point(col * Grid[col, row].Size, row * Grid[col, row].Size);
                }
            }
        }
        public void refreshCells()
        {
            foreach (var cell in Grid)
            {
                cell.refresh();
            }
        }
    }
}
